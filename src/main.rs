#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

use eframe::egui;
use egui::style::Visuals;

fn main() {
    let options = eframe::NativeOptions {
        transparent: true,
        ..eframe::NativeOptions::default()
    };
    eframe::run_native(
        "My egui App",
        options,
        Box::new(|_cc| Box::new(MyApp::default())),
    );
}

struct MyApp {
    name: String,
    age: u32,
}

impl Default for MyApp {
    fn default() -> Self {
        Self {
            name: "Arthur".to_owned(),
            age: 42,
        }
    }
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default()
            .frame(egui::Frame {
                fill: egui::Color32::TRANSPARENT,
                ..egui::Frame::default()
            })
            .show(ctx, |ui| {
                ui.heading("My egui Application");
                ui.horizontal(|ui| {
                    ui.label("Your name: ");
                    ui.text_edit_singleline(&mut self.name);
                });
                ui.add(egui::Slider::new(&mut self.age, 0..=120).text("age"));
                if ui.button("Click each year").clicked() {
                    self.age += 1;
                }
                ui.label(format!("Hello '{}', age {}", self.name, self.age));
            });
    }
    /// Background color for the app, e.g. what is sent to `gl.clearColor`.
    /// This is the background of your windows if you don't set a central panel.
    //fn clear_color(&self, _visuals: &egui::Visuals) -> egui::Rgba {
    //    // NOTE: a bright gray makes the shadows of the windows look weird.
    //    // We use a bit of transparency so that if the user switches on the
    //    // `transparent()` option they get immediate results.
    //    egui::Color32::from_rgba_unmultiplied(12, 12, 12, 100).into()

    //    // _visuals.window_fill() would also be a natural choice
    //}
    fn clear_color(&self, _visuals: &Visuals) -> egui::Rgba {
        egui::Rgba::TRANSPARENT // we set a `CentralPanel` fill color in `demo_windows.rs`
    }
}
